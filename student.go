package main

import (
	"database/sql"
	"errors"
	"time"

	"github.com/lib/pq"
)

// Student estructura de estudiante
type Student struct {
	ID        int
	Name      string
	Age       uint8
	Active    bool
	CreatedAt time.Time
	UpdatedAt time.Time
}

// CreateStudent registra un estudiante en la BD
func CreateStudent(s Student) error {
	q := `INSERT INTO
                students (name, age, active)
                VALUES ($1, $2, $3)`

	intNull := sql.NullInt64{}
	strNull := sql.NullString{}
	// boolNull := sql.NullBool{}

	db := getConnection()
	defer db.Close()

	stmt, err := db.Prepare(q)
	if err != nil {
		return err
	}
	defer stmt.Close()

	if s.Age == 0 {
		intNull.Valid = false
	} else {
		intNull.Valid = true
		intNull.Int64 = int64(s.Age)
	}
	if s.Name == "" {
		strNull.Valid = false
	} else {
		strNull.Valid = true
		strNull.String = s.Name
	}

	r, err := stmt.Exec(strNull, intNull, s.Active)
	if err != nil {
		return err
	}

	i, _ := r.RowsAffected()
	if i != 1 {
		return errors.New("Error: Se esperaba 1 fila afectada")
	}
	return nil
}

// GetAllStudents busca la información de TODOS los estudiantes
func GetAllStudents() (students []Student, err error) {
	q := `SELECT id, name, age, active, created_at, updated_at FROM students`

	timeNull := pq.NullTime{}
	intNull := sql.NullInt64{}
	strNull := sql.NullString{}
	boolNull := sql.NullBool{}

	db := getConnection()
	defer db.Close()

	rows, err := db.Query(q)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		s := Student{}
		err = rows.Scan(
			&s.ID,
			&strNull,
			&intNull,
			&boolNull,
			&s.CreatedAt,
			&timeNull,
		)
		if err != nil {
			return
		}
		s.Name = strNull.String
		s.Age = uint8(intNull.Int64)
		s.Active = boolNull.Bool
		s.UpdatedAt = timeNull.Time

		students = append(students, s)
	}
	return students, nil
}

// UpdateStudent permite actualizar un estudiante de la BD
func UpdateStudent(e Student) error {
        q := `UPDATE students
                SET name = $1, age = $2, active = $3, updated_at = now()
                WHERE id = $4`
        
        db := getConnection()
        defer db.Close()

        stmt, err := db.Prepare(q)
        if err != nil {
                return err
        }
        defer stmt.Close()
        
        r, err := stmt.Exec(e.Name, e.Age, e.Active, e.ID)
        if err != nil {
                return err
        }

        i, _ := r.RowsAffected()
        if i != 1 {
                return errors.New("Error: Se esperaba 1 fila afectada")
        }
        return nil
}

// DeleteStudent permite borrar un estudiante de la BD
func DeleteStudent(id int) error {
        q := `DELETE FROM students WHERE id = $1`

        db := getConnection()
        defer db.Close()

        stmt, err := db.Prepare(q)
        if err != nil {
                return err
        }
        defer stmt.Close()

        r, err := stmt.Exec(id)
        if err != nil {
                return err
        }

        i, _ := r.RowsAffected()
        if i != 1 {
                return errors.New("Error: Se esperaba 1 fila afectada")
        }
        return nil
}